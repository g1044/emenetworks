<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Company;
use App\Models\Companygrowth;


class CompanyController extends Controller
{

    public $host;
    public $content_type;
    public $apikey;
	public $website;

    public function __construct()
	{
		$this->host = "https://financialmodelingprep.com/api/v3/";
        $this->content_type = "application/json";
        $this->apikey = "437a2e04c35dfb2b5e82aec6e79eba54";
		$this->website="https://financialmodelingprep.com/";
	}


    /**
     * Fetching data from API and saving company information into database
     *
     * @return void
     */
    public function companyprofile()
    {
        $response = Http::get($this->host.'profile/AAPL?apikey='.$this->apikey);
        //$companydata = json_decode($response->body(),TRUE);
        $companydata = json_decode($response->getBody()->getContents(), true);
        //return $companydata;
        if(isset($companydata))
        {
            foreach($companydata as $company)
        {
            Company::firstOrCreate([
                'symbol'=>$company['symbol'],
            'companyName'=>$company['companyName'],
            'price'=>$company['price'],
            'mktCap'=>$company['mktCap'],
            'cik'=>$company['cik']
            ]);
        }
            echo "Company Profile Saved";
        }
        else {
            echo "Company Not Exists";
        }
    }

    /**
     * Checking the financial growth of a particular company
     * If data exits then insert data into database
     *
     * @return void
     */
    public function financialgrowth()
    {
        $response = Http::get($this->host.'financial-growth/AAPL?limit=20&apikey='.$this->apikey);
        //$companydata = json_decode($response->body(),TRUE);
        $companydata = json_decode($response->getBody()->getContents(), true);
        //return $companydata;

        if(isset($companydata))
        {
            foreach($companydata as $company)
        {
            Companygrowth::updateOrInsert([
            'date'=>$company['date'],
            'symbol'=>$company['symbol'],
            'revenueGrowth'=>$company['revenueGrowth'],
            'epsGrowth'=>$company['epsgrowth']
            ]);
        }
            echo "Company Financial Growth Data Saved";
        }
        else {
            echo "Company Not Exists";
        }

    }




}
