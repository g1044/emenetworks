<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;
    public $table = "companyprofile";
    protected $fillable = [
        'symbol',
        'companyName',
        'price',
        'mktCap',
        'cik'
    ];
}
