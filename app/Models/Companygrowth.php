<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Companygrowth extends Model
{
    use HasFactory;
    public $table = "companygrowth";
    protected $fillable = [
        'date',
        'symbol',
        'revenueGrowth',
        'epsGrowth',
    ];
}
