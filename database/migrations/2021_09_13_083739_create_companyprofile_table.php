<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyprofileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companyprofile', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('symbol');
            $table->string('companyName');
            $table->float('price',10,2);
            $table->bigInteger('mktCap');
            $table->string('cik');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companyprofile');
    }
}
