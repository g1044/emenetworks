<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanygrowthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companygrowth', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->string('symbol');
            $table->double('revenueGrowth',10,8);
            $table->double('epsGrowth',10,8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companygrowth');
    }
}
